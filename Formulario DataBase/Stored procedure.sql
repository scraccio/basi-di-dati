--STORED PROCEDURES-----------------------------------------------------
--una procedura memorizzata è un insieme di istruzioni SQL che vengono memorizzate nel server con un nome che le identifica; tale nome consente in seguito di rieseguire l’insieme di istruzioni facendo semplicemente riferimento ad esso (come i metodi)

--La procedura viene assegnata ad un database al momento della creazione, ed i nomi referenziati al suo interno si riferiranno allo stesso database, a meno che non siano qualificati con un nome di database specifico. In fase di creazione, quindi, se indichiamo il nome della procedura senza specificare il database questa sarà assegnata al db attualmente in uso.

CREATE PROCEDURE nomeProc (IN param1 INT, OUT param2 INT)
SELECT COUNT(*) INTO param2 FROM tabella
WHERE campo1 = param1

--il risultato della query viene memorizzato nel secondo parametro attraverso la clausola INTO.

--ecco come chiamare la procedura e visualizzare il risultato:
CALL nomeProc (5, @a);
SELECT @a;

--con l’istruzione CALL effettuiamo la chiamata della procedura (immaginando che il database attualmente in uso sia lo stesso a cui la procedura è associata), passando il valore 5 come parametro di input e la variabile @a come parametro di output, nel quale verrà memorizzato il risultato. La SELECT successiva visualizza il valore di tale variabile dopo l’esecuzione

--esempio di procedura complessa

DELIMITER //
CREATE PROCEDURE procedura1 (param1 INT, param2 CHAR(3),
        OUT param3 INT)
BEGIN
        DECLARE finito INT default 0;
        DECLARE a INT;
        DECLARE b CHAR(50);
        DECLARE cur1 CURSOR FOR SELECT id,nome
                FROM clienti WHERE cat = param2;
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'
                SET finito = 1;
        OPEN cur1;
        SET param3 = 0;
        FETCH cur1 INTO a,b;
        ciclo: WHILE NOT finito DO
                IF param3 < param1 THEN
                        SET param3 = param3 + 1;
                        FETCH cur1 INTO a,b;
                ELSE
                        LEAVE ciclo;
                END IF;
        END WHILE ciclo;
END; //
DELIMITER ;

--DELIMITER serve al client mysql per modificare il normale delimitatore delle istruzioni, che sarebbe il punto e virgola
--il codice da eseguire deve essere racchiuso tra le clausole BEGIN ed END
--troviamo tre istruzioni DECLAREche dichiarano altrettante variabili, quindi la dichiarazione di un cursore, infine la dichiarazione di un HANDLER per l’SQLSTATE 02000
--la prima vera operazione è l’apertura del cursore (OPEN), che esegue la query associata utilizzando uno dei parametri di input.
--FETCH fa la lettura della prima riga di risultato della query, i cui valori sono assegnati alle variabili a e b.
--ciclo WHILE (al quale abbiamo assegnato proprio il nome ‘ciclo’) che viene eseguito fintanto che il valore di ‘finito’ è falso, cioè è uguale a zero come da inizializzazione. Tale valore verrà modificato nel momento in cui non ci sono più righe da leggere, come specificato con l’handler
--all’interno del ciclo si verifica se la variabile ‘param3′ ha già raggiunto il valore di ‘param1′, che era l’altro parametro in input: in questo caso l’istruzione LEAVE consente di abbandonare il ciclo stesso; in caso contrario si incrementa la variabile e si esegue una nuova lettura del cursore, sempre riportando i risultati nelle variabili a e b (l’esempio è solo dimostrativo, in quanto non utilizziamo mai questi valori).
--Il ciclo quindi termina quando sono finite le righe del cursore oppure quando ne abbiamo lette tante quante indicate dal primo parametro, a seconda di quale dei due eventi si verifica prima.

--STORED FUNCTIONS-----------------------------------------------------
--simili alle stored procedures, ma hanno uno scopo più semplice, cioè quello di definire vere e proprie funzioni che restituiscono un solo valore (come MAX, MIN, AVG,...)

CREATE FUNCTION nome ([parametro[,...]])
RETURNS tipo
[SQL SECURITY { DEFINER | INVOKER }] corpo
//parametri:
nomeParametro tipo

--Rispetto alle stored procedures, vediamo che si aggiunge la clausola RETURNS che specifica che tipo di dato la funzione restituisce. Inoltre nella lista dei parametri non esiste distinzione fra input e output, in quanto i parametri sono solo in input.


http://www.html.it/pag/32156/stored-procedures-e-stored-functions/